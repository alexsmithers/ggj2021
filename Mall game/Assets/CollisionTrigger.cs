﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionTrigger : MonoBehaviour
{
    public UnityEvent m_onCollision;

    void OnTriggerEnter2D(Collider2D collider)
    {
        m_onCollision.Invoke();
    }
}
