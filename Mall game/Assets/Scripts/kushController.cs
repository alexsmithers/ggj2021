﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kushController : MonoBehaviour
{
    public Vector2 m_xBounds;
    public Vector2 m_yBounds;

    public Vector2 m_direction;

    public float m_speed = 1.0f;

    public int m_state = 0;
    public Vector2 m_startPoint;

    public TimelineManager m_timelineMgr;

    public ConversationCustom m_kushConvo;

    // Start is called before the first frame update
    void Start()
    {
        m_startPoint = transform.localPosition;
        StartChase();
    }

    
    public void SetState(int state)
    {
        m_state = state;
    }

    void StartChase()
    {
        m_direction = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        m_direction.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        if( m_state == 1)
        {
            if( transform.localPosition.x > m_xBounds[1] || transform.localPosition.x < m_xBounds[0] )
            {
                m_direction = new Vector2(Mathf.Sign( m_startPoint[0] - transform.localPosition[0] )  * Random.Range(0.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                m_direction.Normalize();

                transform.localScale = new Vector3(Mathf.Abs(transform.localScale[0]) * Mathf.Sign(m_direction[0]), transform.localScale[1], transform.localScale[2]);

            }
            if (  transform.localPosition.y > m_yBounds[1] || transform.localPosition.y < m_yBounds[0]){
                m_direction = new Vector2( Random.Range(-1.0f, 1.0f), Mathf.Sign(m_startPoint[1] - transform.localPosition[1]) * Random.Range(0.0f, 1.0f) );
                m_direction.Normalize();

                transform.localScale = new Vector3(Mathf.Abs(transform.localScale[0]) * Mathf.Sign(m_direction[0]), transform.localScale[1], transform.localScale[2]);
            }
            

            transform.localPosition += new Vector3( m_direction[0], m_direction[1], 0) * Time.deltaTime * m_speed;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        m_state = 2;
        //m_kushConvo.TriggerConversation();
        m_timelineMgr.UpdateTimeline(-3);
    }

}
