﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour{

public float speed;
public SpriteRenderer spriteRenderer;
public Rigidbody2D target;

    // Start is called before the first frame update
    
    
void Update (){
speed=1f+0.5f*Vector2.Distance(transform.position, target.position);

    this.spriteRenderer.flipX = this.transform.position.x > target.position.x;


     if(Vector2.Distance(transform.position, target.position) > .5){
     
     
        transform.position = Vector2.MoveTowards(transform.position,target.position, speed * Time.deltaTime);
}
}
}
