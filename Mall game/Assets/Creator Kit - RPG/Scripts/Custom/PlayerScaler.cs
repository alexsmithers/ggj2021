﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScaler : MonoBehaviour
{
    public float m_scale = 1.0f;

    public void OnTrigger()
    {
        GameObject player = GameObject.Find("CharacterGGJ");
        player.transform.localScale = new Vector3(m_scale, m_scale, 1.0f);
    }
}
