﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RPGM.Core;
using RPGM.Gameplay;

public class ConversationCustom : MonoBehaviour
{
    [System.Serializable]
    public struct TimelineSaying
    {
        public string text;
        public int timelineStart;
        public int nexTimeline;
        public UnityEvent m_onConversation;
        public UnityEvent m_onConversationComplete;
    }

    public GameObject m_conversationAnchor;
    public TimelineSaying[] m_sayings;

    TimelineManager m_timelineManager;
    public int m_sayingId = -1;

    UnityEvent m_onCurConversationComplete = null;

    public void Start()
    {
        m_timelineManager = GameObject.FindObjectOfType<TimelineManager>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        TriggerConversation();
        
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        TriggerConversation();
    }
    
    void LateUpdate(){
        if( m_onCurConversationComplete != null){
            if(Input.GetKeyDown(KeyCode.Space)){
                m_onCurConversationComplete.Invoke();
                m_onCurConversationComplete = null;
            }
        }
    }

    public void TriggerConversation()
    {
        var ev = Schedule.Add<RPGM.Events.ShowConversation>();
        
        ev.gameObject = ( m_conversationAnchor != null)?m_conversationAnchor:gameObject;
        TimelineSaying s = GetCurSaying();
        m_onCurConversationComplete = s.m_onConversationComplete;
        ev.flatConversation = s.text;
        s.m_onConversation.Invoke();
        if( s.nexTimeline >= 0)
        {
            m_timelineManager.UpdateTimeline(s.nexTimeline);
        }
    }

    string GetCurText()
    {
        return GetCurSaying().text;
    }

    TimelineSaying GetCurSaying() {

        if( m_sayings.Length == 0) { return new TimelineSaying(); }

        if( m_sayingId >= 0 && m_sayingId < m_sayings.Length){
            return m_sayings[m_sayingId];
        }

        int curTimeline = m_timelineManager.GetTimelineIndex();

        
        int foundIndex = -1;
        int closestDist = int.MaxValue;
        for ( int i = 0; i < m_sayings.Length; i++)
        {
            int sayingTime = m_sayings[i].timelineStart;
            int thisDist = Mathf.Abs(sayingTime - curTimeline);
            if ( sayingTime <= curTimeline && thisDist < closestDist)
            {
                closestDist = thisDist;
                foundIndex = i;
            }
        }
        if( foundIndex == -1) { return new TimelineSaying(); }
        return m_sayings[foundIndex];
    }

    public void SetSayingId(int sayingId){
        m_sayingId = sayingId;
    }
}
